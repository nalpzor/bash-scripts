#!/bin/sh

#Creates a basic firewall for a web server

iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT

iptables -A INPUT -p tcp --dport 22 -j ACCEPT

iptables -A INPUT -i lo -j ACCEPT

iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

iptables -P INPUT DROP
iptables -P FORWARD DROP


iptables-save > /etc/firewall

mkdir -p /etc/network/if-pre-up.d/
touch /etc/network/if-pre-up.d/firewall
chmod +x /etc/network/if-pre-up.d/firewall

apt update && apt install -y fail2ban

printf "#!/bin/sh \n/sbin/iptables-restore < /etc/firewall" > /etc/network/if-pre-up.d/firewall
